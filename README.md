# HTCT - IP Monitoring using Netlink 
This program monitors all changes in the Linux networking subsystem.
It detects when an IP address is added or deleted, routing tables changed, etc.
This implementation of network control utilizes the Netlink socket-based 
technology to "subscribe" to network notifications from the Kernel to the userspace.

# Common Protocols 
All available protocols can be found in documentation, here is list of the ones used most often:

    NETLINK_ROUTE          — Routing and link information, monitoring and configuration routines
    NETLINK_FIREWALL       — Transfer packets to userspace from the firewall
    NETLINK_INET_DIAG      — Information about sockets of various protocol families
    NETLINK_NFLOG          — Netfilter/iptables ULOG
    NETLINK_SELINUX        — SELinux event notifications
    NETLINK_NETFILTER      — Communitcaions with netfilter subsystem
    NETLINK_KOBJECT_UEVENT — Get kernel messages
    NETLINK_USERSOCK       — Reserved for user defined protocols

# Netlink Resources

https://www.linuxjournal.com/article/7356
https://www.linuxjournal.com/article/8498
http://man7.org/linux/man-pages/man7/netlink.7.html
http://man7.org/linux/man-pages/man7/rtnetlink.7.html
http://man7.org/linux/man-pages/man3/netlink.3.html
http://man7.org/linux/man-pages/man3/rtnetlink.3.html
https://linux.die.net/man/3/netlink
https://linux.die.net/man/3/htonl
https://linux.die.net/man/3/if_indextoname