/**
 * This program monitors all changes in the Linux networking subsystem.
 * It detects when an IP address is added or deleted, routing tables changed, etc.
 * This implementation of network control utilizes the Netlink socket-based 
 * technology to "subscribe" to network notifications from the Kernel to the userspace.
 * 
 * @author  Gerardo Enrique Alvarenga
 * @version 1.0
 * @since   June 24, 2019
 * 
 */

#include <iostream>
#include <sstream>
#include <thread>
#include <stdio.h>
#include <string.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <unistd.h>
#include <netinet/in.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <net/if.h>

/**
 * This function detects any changes in the IPv4 addresses attached to the 
 * available interfaces of the host device using RTNETLINK to communicate
 * with the kernel.
 * 
 */
int net_route_monitoring() {
	std::cout << ("Starting IP Monitoring") << std::endl;
  		/* Listen for IP changes */
    int sock; 					   /* Holds the file decriptor of the netlink socket */
    int len;
    char buffer[4096];             /* Message buffer */
    struct sockaddr_nl addr;       /* Local Address struct */
    struct nlmsghdr *net_link_msg; /* Struct containing the payload and header of the message */

	/* 	
	 *	AF_NETLINK    - Netlink domain
	 *	SOCK_RAW      - Raw socket
	 *	NETLINK_ROUTE - Required protocol.
	 * 
	 * 	Check README.md for other available protocols 
	 */
    if ((sock = socket(PF_NETLINK, SOCK_RAW, NETLINK_ROUTE)) < 0) { /* Attempt to create the netlink socket - unattached endpoint for kernel comm */
        std::cout << ("Monitoring could not open NetLink Route socket") << std::endl;
		return -1;
    }
    memset(&addr, 0, sizeof(addr));
	
    addr.nl_family = AF_NETLINK;         /* Set protocol family - Set to 0 (AF_UNSPEC) if listening to IPv4 and IPv6 is needed */
    addr.nl_groups = RTMGRP_IPV4_IFADDR; /* Set to listen to IPv4 address events */
	addr.nl_pid    = getpid(); 			 /* Set ID using the current process ID */
    addr.nl_pad    = 0;

    if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) == -1) { /* Attempt to bind the socket to local address */
        std::cout << ("Unable to bind to socket") << std::endl;
		close(sock);
		return -1;
    }

 	net_link_msg = (struct nlmsghdr*) buffer;
    while ((len = recv(sock, net_link_msg, 4096, 0)) > 0) {
        while ((NLMSG_OK(net_link_msg, len)) && (net_link_msg->nlmsg_type != NLMSG_DONE)) {   /* Check if the netlink message is suitable for parsing */
            if (net_link_msg->nlmsg_type == RTM_NEWADDR) {  /* Check for new addresses in the selected group */
                struct ifaddrmsg *ifa = (struct ifaddrmsg*) NLMSG_DATA(net_link_msg); /* Get information about changes in network interface */
                struct rtattr *rth = IFA_RTA(ifa); /* Structure holding optional routing information */
                int rtl = IFA_PAYLOAD(net_link_msg);

                while (rtl && RTA_OK(rth, rtl)) { /* Check if nelink message is poiting to valid routing attributes */
					/* Here we have found the fist chunk of the message. Check the        *
					 * the type. For more information on the different types see man(7)   *
					 * rtnetlink.                                                         *
					 *                                                                    *
					 * Attributes                                                         *
					 * rta_type        value type             description                 *
					 * -------------------------------------------------------------      *
					 * IFA_UNSPEC      -                      unspecified.                *
					 * IFA_ADDRESS     raw protocol address   interface address           *
					 * IFA_LOCAL       raw protocol address   local address               *
					 * IFA_LABEL       asciiz string          name of the interface       *
					 * IFA_BROADCAST   raw protocol address   broadcast address.          *
					 * IFA_ANYCAST     raw protocol address   anycast address             *
					 * IFA_CACHEINFO   struct ifa_cacheinfo   Address information.        */
                    if (rth->rta_type == IFA_LOCAL) {
                        uint32_t ipaddr = htonl(*((uint32_t*) RTA_DATA(rth))); /* Convert from host byte order to network byte order, given by RTA data which returns a pointer to the start of the data attribute (Interface's IP) */
                        char name[IFNAMSIZ]; /* Stores the interface's name */
                        if_indextoname(ifa->ifa_index, name); /* Get the name of the network interface corresponding to the interface index (ifa_index) and stores it in the name param. */
                        
						std::stringstream ss;
                        ss << ((ipaddr >> 24) & 0xff) << "."
                           << ((ipaddr >> 16) & 0xff) << "."
                           << ((ipaddr >> 8) & 0xff) << "."
                           << (ipaddr & 0xff);
                        std::string str = ss.str();

                        std::cout << "Adapter: " << name << (" acquired new IP Address: " + str) << std::endl;

                        // TODO: Here we can trigger an action based on the new IP if needed
                    }
                    rth = RTA_NEXT(rth, rtl); /* Get the next attribute of the structure */
                }
            }
            net_link_msg = NLMSG_NEXT(net_link_msg, len); /* Get the next netlink message */
        }   
    }
}

int main() {
	std::cout << "HTCT - Network programming example" << std::endl;
	std::thread net_monitor_thread = std::thread(&net_route_monitoring);
	return 0;
}